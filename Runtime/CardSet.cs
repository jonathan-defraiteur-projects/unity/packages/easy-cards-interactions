﻿using UnityEngine;

namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [CreateAssetMenu(menuName = "ECI/Set", fileName = "CardSet", order = 10)]
    public class CardSet : ScriptableObject
    {
        public Sprite back;
        public Card[] cards;
    }
}
