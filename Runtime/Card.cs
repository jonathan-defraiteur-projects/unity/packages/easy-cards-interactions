namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [System.Serializable]
    public class Card
    {
        public CardGraphic graphic;
        public CardData data;
    }
}