using UnityEngine;

namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [System.Serializable]
    public class CardGraphic
    {
        public Sprite face;
    }
}