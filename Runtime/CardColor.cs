using UnityEngine;

namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [CreateAssetMenu(menuName = "ECI/Color", fileName = "CardColor", order = 20)]
    public class CardColor : ScriptableObject
    {
    }
}