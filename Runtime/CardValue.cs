using UnityEngine;

namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [CreateAssetMenu(menuName = "ECI/Value", fileName = "CardValue", order = 20)]
    public class CardValue : ScriptableObject
    {
    }
}