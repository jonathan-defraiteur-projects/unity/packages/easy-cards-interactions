using UnityEngine;

namespace JonathanDefraiteur.EasyCardsInteractions.Runtime
{
    [System.Serializable]
    public class CardData
    {
        public CardColor color;
        public CardValue value;

        public string name => $"{value.name}_{color.name}";
    }
}